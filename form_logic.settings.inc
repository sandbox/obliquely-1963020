<?php

/**
 * Form for Form Logic configuration (global settings).
 * 
 * @package AdminInterface
 * 
 */
function form_logic_settings_form() {
  $form = array();

  // make conditional on their being a permission set, such as
  // form_logic_administer
  
  $form['form_logic__verbose'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verbose logging'),
    '#default_value' => variable_get('form_logic__verbose', TRUE),
    '#description' => t('Display errors when callback functions are missing. '
    . 'If not set, the module will fail to execute conditions silently. '
    . 'For use by developers.'),
  );
  
  return system_settings_form($form); // saves the settings automatically
}

