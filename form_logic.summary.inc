<?php

/**
 * @file
 * Utility functions to produce user friendly summaries of Form Logic rules
 *
 *
 * @package BuiltInComponents
 * @author Matthew Elton (obliquely)
 *  
 *
 */


/**
 * Produce a user friendly string that describes the a rule
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupport
 * 
 * @param $rule
 *  An array capturing all the rule condition and action details
 *
 * @param $bundle
 *  Need to know this for case where we need to identify field labels
 *
 * @return string
 */

function _form_logic_summary_for_rule($rule, $bundle) {

  $disable = '';

  if (isset($rule['more']['enabled'])) {
    if (!$rule['more']['enabled']) {
      $disable = ' disable';
    }
  }

  $prefix = '<div class="form_logic_rule_element' . $disable . '">';
  $suffix = '</div>';

  $condition_summary = _form_logic_validation_prefix_for_rule($rule, $bundle);

  $first = TRUE;
  $action_summary = '';
  foreach ($rule['actions'] as $sub_rule) {

    if (!$first) {
      $action_summary .= ' <strong>and</strong> ';
    }

    $action_summary .=
        _form_logic_action_summary_for_rule($sub_rule, $bundle);
    $first = FALSE;
  }

  $stop = '.';

  if (strlen($action_summary) > 6 && substr($action_summary, -6) === '</div>') {
    $stop = '';
  }

  $action_summary .= $stop;

  $guard_clause = '';

  if (isset($rule['more']['guard_clause'])) {
    if ($rule['more']['guard_clause']) {
      $guard_clause = '<p><em>'
          . t('Stop checking rules if this rule succeeds.')
          . '</em></p>';
    }
  }

  return $prefix
      . $condition_summary
      . $action_summary
      . $guard_clause
      . $suffix;
}

/**
 * Provide a human readable summary of the **condition** part of a rule
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupport
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */
function _form_logic_validation_prefix_for_rule($rule, $bundle) {
  $condition_summary = '';

  if (isset($rule['more']['name']) && strlen($rule['more']['name']) > 0) {
    $condition_summary .= '<p><em>'
        . $rule['more']['name'] . '</em></p>';

    if (isset($rule['more']['show_name_only'])) {
      if ($rule['more']['show_name_only']) {
        return $condition_summary;
      }
    }
  }

  $first = TRUE;
  $isAlways = FALSE;
  foreach ($rule['conditions'] as $key => $sub_rule) {

    $isAlways = ($sub_rule['condition'] === 'always');

    if (!$first) {

      if ($isAlways) {
        $condition_summary .= ' <strong> ' . t('and always') . '</strong> ';
      }
      else {
        $condition_summary .= ' <strong>' .
            t('and if') .
            '</strong> ';
      }
    }
    else {
      if ($isAlways) {
        $condition_summary .= ' <strong> ' . t('Always') . '</strong> ';
      }
      else {
        $condition_summary .= '<strong>'
            . t('If')
            . '</strong> ';
      }
    }

    if (!$isAlways) {
      $condition_summary .=
          _form_logic_condition_summary_for_rule($sub_rule, $bundle);
    }
    $first = FALSE;
  }


  $then = '';

  if (!$isAlways) {
    $then = ', <strong>' . t('then') . '</strong> ';
  }


  if (strlen($condition_summary) > 6 && substr($condition_summary, -6) === '</div>') {
    $then = '<strong>' . t('then') . '</strong> ';
  }

  $condition_summary .= $then;

  return $condition_summary;
}

/**
 * Produce a user friendly string that describes condition part of a rule
 *
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupport
 * 
 * @param $rule
 *  An array capturing all the rule condition and action details
 *
 * @return
 *  A string
 */

function _form_logic_condition_summary_for_rule($rule, $bundle) {

  $callback = '_form_logic_summary_part_' . $rule['condition'];

  if (_form_logic_function_check($callback)) {
    return $callback($rule, $bundle);
  }
}

/**
 * Produce a human friendly string that describes the action part of a rule
 * 
 * Generates a callback of the form:
 *  _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupport
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */
function _form_logic_action_summary_for_rule($rule, $bundle) {

  $callback = '_form_logic_summary_part_' . $rule['action'];

  if (!_form_logic_function_check($callback)) {
    return t('&lt;Unknown action&gt;');
  }

  return $callback($rule, $bundle);
}



/**
 * Produce a human friendly string for 'no_op' action
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */
function _form_logic_summary_part_no_op($rule, $bundle) {
  return t('do nothing');
}

/**
 * Produce a human friendly string for 'always' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */
function _form_logic_summary_part_always($rule, $bundle) {
  return t('eggs is eggs');
}

/**
 * Produce a human friendly string for 'user has role' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */
function _form_logic_summary_part_user_has_role($rule, $bundle) {

  if (!isset($rule['roles']) || count($rule['roles']) == 0) {
    return t('user has the role: &lt;no role specified&gt;');
  }

  if (count($rule['roles']) == 1) {
    $roles = user_roles();
    $first = $rule['roles'][key($rule['roles'])];
    $role_name = '<em>' .
        $roles[$first]
        . '</em>';

    return t('user has the role ') . $role_name;
  }

  $roles = user_roles();
  $role_names = array(
  );
  foreach ($rule['roles'] as $role_key) {
    $role_names[] = '<em>' .
        $roles[$role_key]
        . '</em>';
  }

  return t('user has one of the roles: ') . implode(", ", $role_names);
}

/**
 * Utility to show return target field, with bundle appropriate label.
 *  
 * Used to build up other conditions.
 *
 * @package BuiltInComponents
 * @subpackage SummarySupport
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */

function _form_logic_summary_part__target($rule, $bundle) {

  if (!isset($rule['target'])) {
    return '<em>&lt;' . t('no target specified') . '&gt;</em>';
  }


  $target_field = $rule['target']; // need a friendly name
  $instance = field_info_instance('node', $target_field, $bundle);
  return '<em>' . $instance['label'] . '</em> ';
}

/**
 * Produce a human friendly string for 'target had content' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */

function _form_logic_summary_part_target_has_content($rule, $bundle) {

  $summary = _form_logic_summary_part__target($rule, $bundle);
  
  $match = $rule['match_content'];
  $type = $match['match_type'];
  $text = $match['match_parameter_text'];
  
  $case = '';
  if (isset($match['match_parameter_case']))
  {
    $caseSensitive = $match['match_parameter_case'];
    
    if ($caseSensitive) {
      $case = 'Case sensitive';
    }
    else
    {
      $case = 'Ignore case';
    }
  }

  module_load_include('inc', 'form_logic', 'form_logic.built_in_options');

  $match_options = _form_logic_match_options();
  $match_summary = $match_options[$type]['summary'];

  $match_summary_parameter = ($text != '') ?
      $text : '&lt;empty&gt;';

  return
      $summary
      . $match_summary
      . '<div class="form_logic_summary_parameter">'
      . $match_summary_parameter 
      . '<div class="form_logic_case_sensitive">'. $case 
      . '</div>'
      . '</div>';
}


/**
 * Produce a human friendly string for 'target is selected' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */

function _form_logic_summary_part_target_is_selected($rule, $bundle) {

  $summary = _form_logic_summary_part__target($rule, $bundle);
  $parameter = $rule['on_off']['on_off_state'];

  $on_off = 'Unknown';

  if ($parameter == 0) {
    $on_off = 'Off / False / Un-ticked';
  }
  else {
    $on_off = 'On / True / Ticked';
  }
  return
      $summary
      . 'is'
      . '<div class="form_logic_summary_parameter">'
      . $on_off
      . '</div>';
}

/**
 * Produce a human friendly string for 'field not empty' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */

function _form_logic_summary_part_valid_field_not_empty($rule, $bundle) {

  $target_string = _form_logic_summary_part__target($rule, $bundle);
  return $target_string . ' '
      . t('must not be empty');
}


/**
 * Produce a human friendly string for 'field is empty' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */

function _form_logic_summary_part_valid_field_is_empty($rule, $bundle) {
  $target_string = _form_logic_summary_part__target($rule, $bundle);
  return $target_string . ' '
      . t('must be empty');
}


/**
 * Produce a human friendly string for 'field has content' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */

function _form_logic_summary_part_valid_field_has_content($rule, $bundle) {

  $summary = t('require that') . ' ';
  $summary .= _form_logic_summary_part_target_has_content($rule, $bundle);
  return $summary;
}


/**
 * Produce a human friendly string for 'target has term' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */

function _form_logic_summary_part_target_has_term($rule, $bundle) {
  module_load_include('inc', 'form_logic', 'form_logic.built_in_options');

  $target_string = _form_logic_summary_part__target($rule, $bundle);

  // get term names from term ids
  $term_objects = taxonomy_term_load_multiple($rule['terms']);

  $match_types = _form_logic_taxonomy_match_options();

  if (count($term_objects) == 1) {
    $match = $match_types[$rule['terms_options']]['summary_singular'];
  }
  else {
    $match = $match_types[$rule['terms_options']]['summary_plural'];
  }

  $target_string .= 'is set to ' . $match . ' ';

  if (!isset($rule['terms']) || !$rule['terms']) {
    return $target_string . t('&lt;no term specified&gt;');
  }


  foreach ($term_objects as $term_object) {
    $terms[] = "<em>" . $term_object->name . "</em>";
  }

  $expanded_terms = implode(", ", $terms);

  return $target_string . $expanded_terms;
}

/**
 * Produce a human friendly string for 'target is within numerical range term' 
 * condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */

function _form_logic_summary_part_target_is_within_numerical_range($rule,
    $bundle) {

  $lower = t('anything');

  if (isset($rule['numerical_range']['lower'])) {
    $lower = $rule['numerical_range']['lower'];
  }

  $upper = t('anything');

  if (isset($rule['numerical_range']['upper'])) {
    $upper = $rule['numerical_range']['upper'];
  }

  $target_field = $rule['target'];
  $instance = field_info_instance('node', $target_field, $bundle);

  $summary = '<em>' . $instance['label'] . '</em> ';

  $summary .= t('is within the range');

  $summary .= ' ' . $lower . ' ' . t('to') . ' ' . $upper;

  return $summary;
}

/**
 * Produce a human friendly string for 'target is within date range' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */

function _form_logic_summary_part_target_is_within_date_range($rule, $bundle) {

  $start = strtotime($rule['date_range']['earliest_date']);
  $end = strtotime($rule['date_range']['latest_date']);

  $target_field = $rule['target'];
  $instance = field_info_instance('node', $target_field, $bundle);

  $summary = ' <em>' . $instance['label'] . '</em> ';

  $summary .= "is between: ";
  $summary .= isset($start) ? 'Start: ' . format_date($start) . ' ' : '';
  $summary .= isset($end) ? 'End: ' . format_date($end) . ' ' : '';

  return $summary;
}

/**
 * Produce a human friendly string for show message on form action
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */
function _form_logic_summary_part_message_pre($rule, $bundle) {

  return
      '(before submission) show message: <div class="form_logic_message">'
      . $rule['message_text']
      . '</div>';
}


/**
 * Produce a human friendly string for show message after form submission
 * action
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */
function _form_logic_summary_part_message_post($rule, $bundle) {

  return
      '(after submission) show message: <div class="form_logic_message">'
      . $rule['message_text']
      . '</div>';
}


/**
 * Produce a human friendly string for 'valid_field_has_terms' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */
function _form_logic_summary_part_valid_field_has_terms($rule, $bundle) {

  $target_field = $rule['target'];
  $instance = field_info_instance('node', $target_field, $bundle);

  $summary = "require that <em>" . $instance['label'] . "</em> ";

  $terms = _form_logic_summary_term_ids_to_term_names($rule['terms']);

  $match_types = _form_logic_taxonomy_match_options();

  // supply a default if not set. Defensive (and used during testing)
  if (!isset($rule['terms_options'])) {
    $rule['terms_options'] = 'any';
  }

  if (count($terms) == 1) {
    $match = $match_types[$rule['terms_options']]['summary_singular'];
  }
  else {
    $match = $match_types[$rule['terms_options']]['summary_plural'];
  }

  $summary .= ' is set to ' . $match . ' ';

  $summary .= implode(", ", $terms);

  return $summary;
}


/**
 * Produce a human friendly string for 'is_within_date_range' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */

function _form_logic_summary_part_valid_field_is_within_date_range($rule,
    $bundle) {

// probably want to convert date to unix times when we submit
// the form

  $start = strtotime($rule['date_range']['earliest_date']);
  $end = strtotime($rule['date_range']['latest_date']);

  $target_field = $rule['target'];
  $instance = field_info_instance('node', $target_field, $bundle);

  $summary = "require that <em>" . $instance['label'] . "</em> ";

  $summary .= "is restricted to dates between: ";
  $summary .= isset($start) ? 'Start: ' . format_date($start) . ' ' : '';
  $summary .= isset($end) ? 'End: ' . format_date($end) . ' ' : '';

  return $summary;
}

/**
 * Produce a human friendly string for 'within numerical range' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */
function _form_logic_summary_part_valid_field_is_within_numerical_range($rule,
    $bundle) {

  $lower = '';
  if (!empty($rule['numerical_range']['lower'])) {
    $lower = $rule['numerical_range']['lower'];
  }

  $upper = '';
  if (!empty($rule['numerical_range']['upper'])) {
    $upper = $rule['numerical_range']['upper'];
  }

  _form_logic_dpm($rule, 'num range');
  $target_field = $rule['target'];
  $instance = field_info_instance('node', $target_field, $bundle);

  $summary = "require that <em>" . $instance['label'] . "</em> ";

  $summary .= "is restricted to range: ";

  $summary .= t('Min:') . ' ' . $lower . ' ';

  $summary .= t('Min:') . ' ' . $upper;

  return $summary;
}

/**
 * Produce a human friendly string for 'limit field to terms'
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */
function _form_logic_summary_part_limit_field_to_terms($rule, $bundle) {
  module_load_include('inc', 'form_logic', 'form_logic.built_in_options');
  
  $target_field = $rule['target'];
  $instance = field_info_instance('node', $target_field, $bundle);

  $summary = t('limit') . ' <em>' . $instance['label'] . '</em> ';

  // Get the friendly term names from the term ids
  $terms = _form_logic_summary_term_ids_to_term_names($rule['limit_terms']);

  $match_types = _form_logic_taxonomy_match_options();

  // supply a default if not set. Defensive (and used during testing)
  if (!isset($rule['limit_terms_options'])) {
    $rule['limit_terms_options'] = 'any';
  }

  if (count($terms) == 1) {
    $match = $match_types[$rule['limit_terms_options']]['summary_singular'];
  }
  else {
    $match = $match_types[$rule['limit_terms_options']]['summary_plural'];
  }

  $summary .= t('to') . ' ' . $match . ' ';

  $summary .= implode(", ", $terms);

  return $summary;
}

/**
 * Helper function - takes array of term ids and returns and
 * array of term names with strong styling
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupport
 
 * @param array $term_ids
 * @return string
 * 
 */
function _form_logic_summary_term_ids_to_term_names($term_ids) {

  if (count($term_ids) == 0) {
    return array(
      '&lt;' . t('no term specified') . '&gt;');
  }

  $term_objects = taxonomy_term_load_multiple($term_ids);

  foreach ($term_objects as $term_object) {
    $terms[] = "<em>" . $term_object->name . "</em>";
  }

  return $terms;
}

/**
 * Produce a human friendly string for 'hide field' action
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */
function _form_logic_summary_part_hide_field($rule, $bundle) {

  $target_field = $rule['target'];
  $instance = field_info_instance('node', $target_field, $bundle);

  $summary = "hide <em>" . $instance['label'] . "</em>";

  return $summary;
}

/**
 * Produce a human friendly string for 'limit formats' condition
 * 
 * An implementation of _form_logic_summary_part_CONDITION_OR_ACTION 
 * 
 * The actual feature is not implemented, though this function may be OK.
 * 
 * @package BuiltInComponents
 * @subpackage SummarySupportItem
 * 
 * @param type $rule
 * @param type $bundle
 * @return string
 */
function _form_logic_summary_part_limit_formats($rule, $bundle) {

  $target_field = $rule['target'];
  $instance = field_info_instance('node', $target_field, $bundle);

  $summary = "restrict text formats (filters) for <em>" . $instance['label'] . "</em>";

  return $summary;
}
