<?php
/**
 * From Logic built in option functions
 * 
 * Specifies the built in conditions, actions and match options
 *
 * Available conditions for form logic rules
 * 
 * HOOK to allow other modules to add to this isn't yet wired in
 * 
 * 
 * array elements
 * 
 * Each condition needs a corresponding
 *  _form_logic_summary_component_CONDITION($rule) function
 *  e.g. _form_logic_summary_component_user_has_role
 *  If the function is mising the module will generate error messages in the UI.
 * 
 *      <name of condition> => array(
 *      'title' => t('<User friendly title for condition>'),  // REQUIRED
 *      'parameters' => array('parameter_1', 'parameter_2', ...),  // OPTIONAL
 *      'valid_target_types' => array(<field_type_1>, <field_type_2>, ...) // OPTIONAL
 *      'for_checking' => array('before', 'after'), // REQUIRED - specifies
 *                          // whether check takes place at form at a layout or
 *                          // in the validation phase 
 * )
 *  
 * 
 * Each parameter in the the parameters array needs:
 *  1. a _form_logic_parameter_PARAMETER($rule, $form, $form_state) function.
 *    e.g. _form_logic_parameter_roles
 *  2. a _form_logic_summary_component_PARAMETER ($rule) function
 *    e.g. _form_logic_summary_component_roles
 * 
 * 
 * @package BuiltInComponents
 * @subpackage Specifications 
 * @todo Add hooks so a separate module can add new conditions
 * @file form_logic.built_in.options.inc
 *  
 */


/**
 * Built in conditions
 * 
 * Key is the internal name for the condition
 * 
 * Each condition specification is an array with the following keys:
 * 
 * - title - a translatable string that describes the condition
 * 
 * - check_before a boolean that indicates (??) whether the condition
 *  should be checked ONLY before the form is laid out - this is because
 *  for some conditions checking in the validation phase makes no sense.
 *  This can't be quite right. Need to look at the code and think through
 *  again. At this stage, I'm unclear what problem I was trying to solve.
 * 
 * Checks for the presence of the 'Rules' module and provides an extra
 * condition if it is present. (But the rules condition is not implemented
 * at the moment. Or if it is, it's not implemented...). (Oops!)
 * 
 * 
 * @package BuiltInComponents
 * @subpackage Specifications 
 *
 * 
 * @return array
 */
function _form_logic_available_conditions_info() {
  $conditions =
      array(
        'always' => array(
          'title' => t('Always apply'),
        ),
        'user_has_role' => array(
          'title' => t('User has role(s)'),
          'parameters' => array('roles'),
        ),
         'time_is' => array(
          'title' => t('The time is before/after...'),
          'parameters' => array('date_range'),
        ),
        'target_has_content' => array(
          'title' => t('Target field has specific content'),
          'valid_target_types' => array('text', 'text_long', 'text_with_summary'),
          'parameters' => array('target', 'match_content'),
        ),
         'target_is_selected' => array(
          'title' => t('Target field is on / true / ticked'),
          'valid_target_types' => array('list_boolean'),
          'parameters' => array('target', 'on_off'),
        ),
        'target_has_term' => array(
          'title' => t('Target field has taxonomy term(s)'),
          'valid_target_types' => array('taxonomy_term_reference'),
          'parameters' => array('target', 'terms', 'terms_options'),
        ),
        'target_is_within_date_range' => array(
          'title' => t('Target field is within date / time range'),
          'valid_target_types' => array('date*'),
          'parameters' => array('target', 'date_range'),
        ),
        'target_is_within_numerical_range' => array(
          'title' => t('Target field is within numerical range'),
          'valid_target_types' => array('number*'),
          'parameters' => array('target', 'numerical_range'),
        ),
  );

  // Rules support.
  if (module_exists('rules')) {
    $conditions['rules_event'] =
        array(
          'title' => t('When rule condition is true'),
          'parameters' => array('rules'),
          );
  }

  return $conditions;
}

/**
 * Built in actions
 * 
 * Definitions of Form Logic actions + utility functions to support actions
 *  
 * Key is the internal name of the action.
 * 
 * Each action specification is an array with the following keys:
 * 
 * <pre>
 * title                string  required
 * parameters           array   optional
 * applicable           array   required
 * valid_target_types   array   optional
 * </pre>
 *  
 * @package BuiltInComponents
 * @subpackage Specifications 
 *
 * 
 * @return array
 */

function _form_logic_available_actions_info() {
  $actions =
      array(
        'no_op' => array(
          'title' => t('None'),
          'applicable' => array(),
        ),
        'valid_field_not_empty' => array(
           'title' => t('Require field is not empty'),
          'parameters' => array('target'),
          'applicable' => array('after'),
        ),
         'valid_field_is_empty' => array(
           'title' => t('Require field is empty'),
          'parameters' => array('target'),
          'applicable' => array('after'),
        ),
        'valid_field_has_terms' => array(
          'title' => t('Require field has terms...'),
          'valid_target_types' => array('taxonomy_term_reference'),
          'parameters' => array('target', 'terms', 'terms_options'),
          'applicable' => array('after'),
        ),
         'limit_field_to_terms' => array(
          'title' => t('Limit field to terms'),
          'parameters' => array('target', 'limit_terms'),
          'valid_target_types' => array('taxonomy_term_reference'),
          'applicable' => array('before', 'after'),
        ),
        'valid_field_has_content' => array(
          'title' => t('Require field matches...'),
          'valid_target_types' => array('text*'),
          'parameters' => array('target', 'match_content'),
          'applicable' => array('after'),
        ),
        'valid_field_is_within_date_range' => array(
          'title' => t('Require field is within date / time range'),
          'target_field' => 'YES',
          'parameters' => array('target', 'date_range'),
          'valid_target_types' => array('date*'),
          'applicable' => array('after'),
        ),
        'valid_field_is_within_numerical_range' => array(
          'title' => t('Require field is within numerical range'),
          'target_field' => 'YES',
          'valid_target_types' => array('number*'),
          'parameters' => array('target', 'numerical_range'),
          'applicable' => array('after'),
        ),
        'hide_field' => array(
          'title' => t('Hide field'),
          'parameters' => array('target'),
          'applicable' => array('before'),
        ),
        'message_pre' => array(
          'title' => t('Show a message on the form'),
          'parameters' => array('message_text'),
          'applicable' => array('before'),
        ),
        'message_post' => array(
          'title' => t('Show a message after submission'),
          'parameters' => array('message_text'),
          'applicable' => array('after'),
        ),
        'modify_description' => array(
          'title' => t('Modify description'),
          'parameters' => array('message_text'),
          'applicable' => array('before'),
        ),
        /* 'limit_formats' => array(
          'title' => t('Limit formats'),
          'parameters' => array('target', 'formats'),
          'applicable' => array('before'),
        ), */
  );

  return $actions;
}


/**
 * Built in match options for text fields
 * 
 * Included options:
 * 
 * * contains
 * * does not containt
 * * begins
 * * does not begin
 * * matches regular expression
 *  
 * @package BuiltInComponents
 * @subpackage Specifications 
 *
 * 
 * @return array
 */

function _form_logic_match_options() {

  $options =
      array(
        'contains' => array(
          'name' => t('contains'),
          'parameters' => array('text', 'case'),
          'summary' => t('contains:'),
        ),
        'excludes' => array(
          'name' => t('does not contain'),
          'parameters' => array('text', 'case'),
          'summary' => t('does not contain:'),
        ),
        'begins' => array(
          'name' => t('begins'),
          'parameters' => array('text', 'case'),
          'summary' => t('contains:'),
        ),
        'not_begins' => array(
          'name' => t('does not begin'),
          'parameters' => array('text', 'case'),
          'summary' => t('does not begin:'),
        ),
        'regular_expression' => array(
          'name' => t('matches regular expression'),
          'parameters' => array('text'),
          'summary' => t('match the regular expression:'),
        ),
  );

  return $options;
}


/**
 * Built in match conditions for taxonomy fields
 * 
 * * Any of selected ('any of')
 * * All selected ('all of')
 * * All and only selected ('all and only')
 * * None of selected ('anything except')
 * 
 *
 * @package BuiltInComponents
 * @subpackage Specifications 
 *
 * @return array
 */
function _form_logic_taxonomy_match_options() {

  $options =
      array(
        'any' => array(
          'name' => t('Any of selected'),
          'summary_singular' => t(''),
          'summary_plural' => t('any of')
        ),
        'all' => array(
          'name' => t('All selected'),
          'summary_singular' => t(''),
          'summary_plural' => t('all of')
        ),
        'exactly' => array(
          'name' => t('All and only selected'),
          'summary_singular' => t(''),
          'summary_plural' => t('all and only')
        ),
        'none' => array(
          'name' => t('None of selected'),
          'summary_singular' => t('anything except'),
          'summary_plural' => t('anything except')
        ),
  );

  return $options;
}
