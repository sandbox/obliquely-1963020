<?php

/**
 * Source for built in actions and conditions
 * 
 * Callback functions for built in conditions and actions (+helper functions)
 * @file form_logic.conditions_and_actions.inc
 * 
 * 
 */

/*
 * 
 * _form_logic_check_CONDITION_NAME($rule, $form_state, &$form = NULL)
 * _form_logic_apply_ACTION_NAME($rule, $form_state, &$form = NULL)
 * 
 * For the pre-submission case, we want to pass form so we can modify form;
 * 
 
 */

/**
 * Check whether the condition 'always' is TRUE (it always is).
 * Implementation of  _form_logic_check_CONDITION for 'always'
 * 
 * @package BuiltInComponents
 * @subpackage Conditions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */

function _form_logic_check_always($rule, $form_state, &$form = NULL) {
  return TRUE;
}

/**
 * Check target has a term
 * 
 * Implements *_form_logic_check_CONDITION()* for 'target_has_term'
 * Checks a term reference field to see if it matches condition
 * 
 * @package BuiltInComponents
 * @subpackage Conditions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */
function _form_logic_check_target_has_term($rule, $form_state,
    &$form = NULL) {

  $values = _form_logic_extract_target_values($form_state, $rule);
  $common_values = array_intersect($rule['terms'], $values);

  $terms_options = $rule['terms_options'];

  if ($terms_options === 'any') {
    if (count($common_values) > 0) {
      return TRUE;
    }
    return FALSE;
  }

  if ($terms_options === 'exactly') {
    if (count($values) == count($common_values)
        && count($values) == count($rule['terms'])) {
      return TRUE;
    }
    return FALSE;
  }

  if ($terms_options === 'all') {
    if (count($rule['terms']) >= count($common_values)) {
      return TRUE;
    }
    return FALSE;
  }

  if ($terms_options === 'none') {
    if (count($common_values) == 0) {
      return TRUE;
    }
    return FALSE;
  }

  _form_logic_log('Unknown terms option.');
  return FALSE;
}


/**
 * Check whether the user has a role
 * 
 * Implements _form_logic_check_CONDITION for 'user_has_role'
 * 
 * @package BuiltInComponents
 * @subpackage Conditions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */

function _form_logic_check_user_has_role($rule, $form_state, &$form = NULL) {

  global $user;
  $intersect = array_intersect_key($rule['roles'], $user->roles);

  if (count($intersect) == 0) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Check whether the target field has specified content
 * 
 *  
 * Implements _form_logic_check_CONDITION for 'target_has_content'
 * 
 * @package BuiltInComponents
 * @subpackage Conditions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 * 
 * @return boolean 
 */

function _form_logic_check_target_has_content ($rule, $form_state, &$form = NULL) {
  
  $type = $rule['match_content']['match_type'];
  $parameters = $rule['match_content'];
  unset($parameters['match_type']);

  $callback = '_form_logic_match_type_' . $type;

  if (!_form_logic_function_check($callback)) {
    return '';
  }
  
  $values = _form_logic_extract_target_values($form_state, $rule);
  
  $testResult = $callback($values, $parameters);
  return $testResult['pass'];
}

/**
 * Check whether the target field is On / True / Ticked
 * 
 *  
 * Implements _form_logic_check_CONDITION for 'target_is_selected'
 * 
 * @package BuiltInComponents
 * @subpackage Conditions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 * 
 * @return boolean 
 */

function _form_logic_check_target_is_selected ($rule, $form_state, &$form = NULL) {
  
  $parameter = $rule['on_off']['on_off_state'];

  $values = _form_logic_extract_target_values($form_state, $rule);

  $result = FALSE;
  foreach ($values as $value) {

    if ($parameter == 1) {
      
      
      if ($value == 0) {
        $result = FALSE;
        break;
      }
      $result = TRUE;
    }
    else {
      if ($value !== 0) {
        $result = FALSE;
        break;
      }
      $result = TRUE;
    }
  }
  return $result;
}

/**
 * Check whether a specifed date field is within a specified range
 * 
 * Implements _form_logic_check_CONDITION for 
 * 'valid_field_is_within_date_range'
 * 
 * If there are end dates these are ignored. (The key for end dates is
 * 'value2'.) If the field has multiple entries (or repeating ones) then
 * every entry is checked. If any of the multiple entries fail the test
 * then the overall result is FALSE, i.e. all of the multiple dates must
 * be in range.
 *  
 * @package BuiltInComponents
 * @subpackage Conditions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */

function _form_logic_check_target_is_within_date_range($rule, $form_state, &$form = NULL) {
    
  $lower = '';
  if (!empty($rule['date_range']['earliest_date'])) {
    $lower = $rule['date_range']['earliest_date'];
  }
  
  $upper = '';
  if (!empty($rule['date_range']['latest_date'])) {
    $upper = $rule['date_range']['latest_date'];
  }
  
 $values = _form_logic_extract_target_values($form_state, $rule);
 
 $within_range = TRUE;
 
  foreach($values as $value) {

    if ($value < $lower) {
      $within_range = FALSE;
      break;
    }
    
    if ($value > $upper) {
      $within_range = FALSE;
      break;
    }
  }
  
  if ($within_range) {
    return TRUE;
  }
  
    return FALSE;
}



/**
 * Limit a term reference field to a set of specified terms (after submission)
 * 
 * Implements _form_logic_check_CONDITION for 'limit_field_to_terms'
 * Checks a term reference field to see if it matches condition
 * 
 * @package BuiltInComponents
 * @subpackage Actions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */

function _form_logic_apply_after_limit_field_to_terms($rule, $form_state,
    &$form = NULL) {

  $values = _form_logic_extract_target_values($form_state, $rule);
  $term_options = $rule['limit_terms'];
  
  $disallowed_terms = array_diff($values, $term_options);
  
  if (count($disallowed_terms) > 0)
  {
    return FALSE;
  }
  
  return TRUE;
}

/**
 * Limit a term reference field to a set of specified terms (before submission)
 * 
 * Implements _form_logic_check_CONDITION for 'limit_field_to_terms'
 * Checks a term reference field to see if it matches condition
 * 
 * @package BuiltInComponents
 * @subpackage Actions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */

function _form_logic_apply_before_limit_field_to_terms($rule, $form_state,
    &$form = NULL) {
 
    // see http://drupal.stackexchange.com/questions/25839/best-practice-for-drupal-7-language-key-for-und-in-hook-form-alter
  $language = $form[$rule['target']]['#language'];
  $target = $rule['target'];
  
 // explicitly check for an autocomplete field - it needs to be handled
 // differently
 if (isset($form[$target][$language]['#autocomplete_path']))
 {
    $bundleName = $form['#bundle'];
    
    // modify the autocomplete path so that it will call a Form Logic
    // specific function - provide the bundle name and field name
    // as part of the path. This will allow the custom autocomplete
    // function to recover the list of acceptable terms

    $autoPath = '_form_logic/taxonomy/autocomplete/' . $bundleName . '/' . $target;
    
    $autoPath .= '/' . implode('+', $rule['limit_terms']);
 
    $form[$target][$language]['#autocomplete_path'] = $autoPath;
 
    return;
 }
 
  $options = $form[$target][$language]['#options'];
  
  foreach ($options as $key => $value) {

    if (!in_array($key, $rule['limit_terms'])) {
      unset($options[$key]);
    }

    $form[$target][$language]['#options'] = $options;
  }
}






/**
 * Check whether a specifed field has specified content
 * 
 * Implements _form_logic_check_CONDITION for 'valid_field_has_content'
 * 
 * @package BuiltInComponents
 * @subpackage Conditions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */

function _form_logic_apply_after_valid_field_has_content($rule, $form_state, &$form = NULL) {
  
  $type = $rule['match_content']['match_type'];
  $parameters = $rule['match_content'];
  unset($parameters['match_type']);

  $callback = '_form_logic_match_type_' . $type;

  if (!_form_logic_function_check($callback)) {
    return '';
  }
  
  $values = _form_logic_extract_target_values($form_state, $rule);
  
  return $callback($values, $parameters);
}


/**
 * Check whether a specifed number field is within a specified range
 * 
 * Implements _form_logic_check_CONDITION for 
 * 'valid_field_is_within_numerical_range'
 * 
 * @package BuiltInComponents
 * @subpackage Conditions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */
function _form_logic_apply_valid_field_is_within_numerical_range($rule, $form_state, &$form = NULL) {
  
  $lower = '';
  if (!empty($rule['numerical_range']['lower'])) {
    $lower = $rule['numerical_range']['lower'];
  }
  
  $upper = '';
  if (!empty($rule['numerical_range']['upper'])) {
    $upper = $rule['numerical_range']['upper'];
  }
  
 $values = _form_logic_extract_target_values($form_state, $rule);
 
 $within_range = TRUE;
 
  foreach($values as $value) {

    if ($value < $lower) {
      $within_range = FALSE;
      break;
    }
    
    if ($value > $upper) {
      $within_range = FALSE;
      break;
    }
  }
  
  if ($within_range) {
    return '';
  }
  
    return "must have a range between $lower and $upper";
}



/**
 * Check whether a specifed date field is within a specified range
 * 
 * Implements _form_logic_check_CONDITION for 
 * 'valid_field_is_within_date_range'
 *  
 * @package BuiltInComponents
 * @subpackage Conditions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */

function _form_logic_apply_valid_field_is_within_date_range($rule, $form_state, &$form = NULL) {
    
  $lower = '';
  if (!empty($rule['date_range']['earliest_date'])) {
    $lower = $rule['date_range']['earliest_date'];
  }
  
  $upper = '';
  if (!empty($rule['date_range']['latest_date'])) {
    $upper = $rule['date_range']['latest_date'];
  }
  
 $values = _form_logic_extract_target_values($form_state, $rule);
 
 $within_range = TRUE;
 
  foreach($values as $value) {

    if ($value < $lower) {
      $within_range = FALSE;
      break;
    }
    
    if ($value > $upper) {
      $within_range = FALSE;
      break;
    }
  }
  
  if ($within_range) {
    return '';
  }
  
    return "must have a range between $lower and $upper";
}


/**
 * Check whether a field is not empty, i.e. has some content
 * 
 * Implements _form_logic_check_CONDITION for 
 * 'valid_field_not_empty'
 *  
 * @package BuiltInComponents
 * @subpackage Conditions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */

function _form_logic_apply_valid_field_not_empty($rule, $form_state,
    &$form = NULL) {
  $values = _form_logic_extract_target_values($form_state, $rule);

  $has_content = FALSE;
  foreach ($values as $value) {
    if ($value != '') {
      $has_content = TRUE;
      break;
    }
  }

  if ($has_content) {
    return '';
  }

  return t('is not allowed to be empty.');
}

/**
 * Check whether a field is empty, i.e. has not content
 * 
 * Implements _form_logic_apply_BEFOREAFTER_ACTION for 
 * 'after' 'valid_field_is_empty'
 *  
 * @package BuiltInComponents
 * @subpackage Conditions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 * 
 * @return string error string (if invalid) or '' if all OK
 */

function _form_logic_apply_valid_field_is_empty($rule, $form_state,
    &$form = NULL) {

  $values = _form_logic_extract_target_values($form_state, $rule);

  $has_content = FALSE;
  foreach ($values as $value) {
    if ($value != '') {
      $has_content = TRUE;
      break;
    }
  }

  if (!$has_content) {
    return '';
  }

  return t('must be empty.');
}

 /**
  * Check for 'containing' matches
  * 
  * Implements _form_logic_match_type_MATCH_TYPE for 
  * 'contains'
  * 
  * If there are multiple fields return TRUE if any of the fields
  * contain the relevant text.
  *  
  * @package BuiltInComponents
  * @subpackage Match
  * 
  * @param $values - have forgotten (oops!)
  * @param $parameters - have forgotten (oops!)
  * 
  * @return array {'pass' : TRUE / FALSE, 'report', errorString}
  */

 function _form_logic_match_type_contains($values, $parameters) {
  // could assume strings separated by commas and explode them
  // etc.
  // or have a parameter that determines how the test is done
  // for now it just a straight search for the whole contents
  // of the contains field
  // default is success and no report
  $result = array('pass' => TRUE, 'report', '');

  $needle = $parameters['match_parameter_text'];
  $case_sensitive = $parameters['match_parameter_case'];

  foreach ($values as $value) {

    if ($case_sensitive) {
      if (strpos($value, $needle) === FALSE) {
        continue;
      }
      return $result; // found it, so bail
    }

    if (stripos($value, $needle) === FALSE) {
      continue;
    }
    return $result;  // found it, so bail
  }

  $result['pass'] = FALSE;
  
  if ($case_sensitive) {

    $result['report'] = t('must contain') . ' "' . $needle . '" ' . t('(matching case)');
  }
  else {
    $result['report'] = t('must contain') . ' "' . $needle . '"';
  }

  return $result;
}

/**
 * Check for 'excluding' matches, i.e. field does not contain
 * 
 * Implements _form_logic_match_type_MATCH_TYPE for 
 * 'excludes'
 *  
 * @package BuiltInComponents
 * @subpackage Match
 * 
 * @param $values - have forgotten (oops!)
 * @param $parameters - have forgotten (oops!)
 */

function _form_logic_match_type_excludes($values, $parameters) {
  // could assume strings separated by commas and explode them
  // etc.
  // or have a parameter that determines how the test is done
  // for now it just a straight search for the whole contents
  // of the contains field

  $needle = $parameters['match_parameter_text'];
  $case_sensitive = $parameters['match_parameter_case'];

  foreach ($values as $value) {

    if ($case_sensitive) {
      if (strpos($value, $needle) !== FALSE) {
        return t('must not contain')
            . ' "' . $needle . '" '
            . t('(matching case)');
      }
      continue;
    }

    if (stripos($value, $needle) !== FALSE) {
      return t('must not contain')
          . ' "' . $needle . '"';
    }
    continue;
  }

  return '';
}


/**
 * Show a message on the form (before submission)
 * 
 * Implements _form_logic_apply_before_ACTION for 'message_pre'
 * 
 * @package BuiltInComponents
 * @subpackage Actions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */

function _form_logic_apply_before_message_pre($rule, $form_state, &$form = NULL) {

  if (!isset($form['form_logic_message'])) {
  
  $message = '<div class="form_logic_message">' .  $rule['message_text'] . '<div>';
  
    $form['form_logic_message'] = array(
      '#type' => 'fieldset',
      '#title' => t('Please note'),
      '#weight' => '-100', // try to push to top
      'message' => array(
        '#markup' => $message
      ),
    );

    return;
  }

  // if we already have a message, append this message
  $form['form_logic_message']['message']['#markup'] =
      $form['form_logic_message']['message']['#markup'] . $rule['message_text'];
}


/**
 * Hide a field on the form
 * 
 * Implements _form_logic_apply_before_ACTION for 'hide_field'
 * 
 * @package BuiltInComponents
 * @subpackage Actions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */

function _form_logic_apply_hide_field($rule, $form_state, &$form = NULL) {
  // not hiding so much as erasing from the face of the form
  unset($form[$rule['target']]);
}


/**
 * Show a message after the form has been submitted
 * 
 * Implements _form_logic_apply_after_ACTION for 'message_post'
 * 
 * @package BuiltInComponents
 * @subpackage Actions
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */
function _form_logic_apply_after_message_post($rule, $form_state, &$form = NULL) {

  drupal_set_message($rule['message_text']);
}


/**
 * Limit the text format for a text / longtext field
 * 
 * Implements _form_logic_apply_before_ACTION for 'limit_formats'.
 * **Not implemented**
 * 
 * @package BuiltInComponents
 * @subpackage WorkInProgress
 * 
 * @param $rule a form logic rule
 * @param $form_state drupal form state (forgotten what that is **!**)
 * @param $form array with the specification of the form to be modified
 */
function _form_logic_apply_before_limit_formats($rule, $form_state, &$form = NULL) {

  $element = $form[$rule['target']];
  dpm($element, 'element');

  $element['#after_build'][] = '_form_logic_after_build';
  dpm($element, 'element');

  $form[$rule['target']] = $element;
}

/**
 * Log form state (for testing)
 * 
 * Support function intended to help with limited text formats
 * 
 * **Not implemented** (or, at least, not working)
 * 
 * @package BuiltInComponents
 * @subpackage WorkInProgress
 * 
 * @param type $form
 * @param type $form_state
 */
function _form_logic_after_build($form, $form_state) {
  _form_logic_dpm($form, 'after build form');
  _form_logic_dpm($form_state, 'after build form state');
}

/**
 *  Get values from a form field in a common format
 *
 * Different field types store values in different ways
 * we want to extract into a (reasonably) common pattern
 * 
 * There's a lot of pressure on this function. May need to change approach.
 * 
 * Function might benefit from being explicitly told if this is a BEFORE or
 * AFTER submission case. (Or maybe it's better that it figures it out, because
 * the $form_state can acquire values as a result of a rebuild rather than a
 * submission and what we probably want is just what is most recent.)
 * 
 * Suspect need to have explicit handling for types in order to be able to
 * return a consistent result.
 * 
 * @package BuiltInComponents
 * @subpackage SupportFunctions
 * 
 *  @param array $form_state
 *    form state as returned by the Form API
 *  @param array $rule
 *    a form logic sub rule (will contain the 'target' key)      
 *
 *  @return array
 *    an array of values corresponding to what has been
 *    entered on the form , e.g. strings (for text or number
 *    fields or tids for taxonomy term fields) 
 */
function _form_logic_extract_target_values($form_state, $rule) {
  
// see http://drupal.stackexchange.com/questions/25839/best-practice-for-drupal-7-language-key-for-und-in-hook-form-alter
  
  $logMessage = 'extract target values called: ';
  
  $target = $rule['target'];
  $fieldInfo = field_info_field($target);
  $fieldType = $fieldInfo['type'];
  
  if (!isset($form_state['values'])) {
    // The form has not been submitted. In this case, we read the values
    // out of the node rather than from the form.     
    return _form_logic_extract_node_values($form_state['node'], $target, $fieldType);
  }
  
  return _form_logic_extract_form_value($form_state, $target, $fieldType);
}

function _form_logic_extract_form_value($form_state, $target, $fieldType) {

  $language = $form_state['values']['language'];
  $values = array();

  if (!empty($form_state['values'][$target])) {
    $values = $form_state['values'][$target];
  }

  if ($values[$language]) {
    $values = $values[$language];
  }

  $tidy_values = array();

  // legacy - there was a test for 'target_id' ??

  switch ($fieldType) {

    case 'text_long':
    case 'datetime':
    case 'list_boolean':
      foreach ($values as $value) {
        $tidy_values[] = $value['value'];
      }
      break;
    case 'taxonomy_term_reference':
      foreach ($values as $value) {
        $tidy_values[] = $value['tid'];
      }
      break;

    default:
       $message = 'Function \'_form_logic_extract_form_values\' may not guess correctly '
          . 'how to unpack values from the fields of the type ' . $fieldType
          . '. But it will do its best using what looks like a fair assumption.';

       drupal_set_message($message, 'warning');
      // different fields have different columns
      // if we know no better, privelege the FIRST column
      // (may fail in some cases)
     
     /* if (array_key_exists($first_column, $raw_values[0])) {
        foreach ($raw_values as $value) {
          $tidy_values[] = $value[$first_column];
        }
      }
      */
      
      foreach ($values as $value) {
        if (isset($value['value'])) {
        $tidy_values[] = $value['value'];
        }
      }
  }
  
  return $tidy_values;

}

/**
 * Returns values for a field in an array (normalised across field types)
 * 
 * @param node $node
 * @param string $fieldName
 * @param string $fieldType
 * @return array
 */
function _form_logic_extract_node_values($node, $fieldName, $fieldType)
 {
  $values = field_get_items('node', $node, $fieldName);

  $tidy_values = array();
  
  if (!$values)
  {
    drupal_set_message('fieldName: \'' . $fieldName . '\' fieldType: \'' . $fieldType . '\'' . ' values: ' . 
        'NO VALUE');
    return $tidy_values;
  }
  
  drupal_set_message('fieldName: \'' . $fieldName . '\' fieldType: \'' . $fieldType . '\'' . ' values: ' . print_r($values, TRUE));
  
  switch ($fieldType) {

    case 'text_long':
    case 'datetime':
    case 'list_boolean':
    
      foreach ($values as $value) {
        $tidy_values[] = $value['value'];
      }
      break;
    case 'taxonomy_term_reference':
      foreach ($values as $value)
      {
        $tidy_values[] = $value['tid'];
      }
      break;
      
      
    default:
            drupal_set_message('** reached case default case');


  }
  
  drupal_set_message('extracted from node (field: ' . $fieldName . ' type: ' .
      $fieldType . ') tidy_array: ' . print_r($tidy_values, TRUE));
  
  return $tidy_values;
}
