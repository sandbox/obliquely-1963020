<?php

/**
 * @file
 * Administrative interface for Form Logic
 * 
 * Provide a form that allows for Form Logic rules to be added,
 * edited, and deleted fpr a given bundle. Rules can also be re-ordered
 * by dragging.
 * 
 * The editing of rules is done with code in form_logic.configure.inc
 * 
 * 
 */

/**
 * Enable Toggle
 * 
 * @package AdminInterface
 * 
 * Not sure what this does (oops!)
 * Theris a variable_get here though. That's a clue.
 * 
 * @param type $bundle
 * @param type $rid
 * @return type
 */
function _form_logic_enable_toggle($bundle, $rid) {

  $rules = variable_get('form_logic_' . $bundle, array());
  if (!isset($rules)) {
    return;
  }

  if (!isset($rules[$rid])) {
    return;
  }

  $rules[$rid]['more']['enabled'] = !$rules[$rid]['more']['enabled'];

  variable_set('form_logic_' . $bundle, $rules);
}

/**
 * Admin Form
 * 
 * @package AdminInterface
 * 
 * @param type $form
 * @param type $form_state
 * @param type $bundle
 * @param type $operation
 * @param type $rid
 * @return string
 */

function form_logic_admin_form($form, &$form_state, $bundle, $operation = NULL, $rid = NULL) {

    $bundleAdjusted = str_replace('-', '_', $bundle);

  if (isset($operation)) {
    
    if ($operation === 'enable') {
      _form_logic_enable_toggle($bundleAdjusted, $rid);
    }
    
    // restore URL to original page URL
    drupal_goto('admin/structure/types/manage/' .
        $bundleAdjusted . '/form_logic');
  }

  // Get the label for the content type
  // will need to soup this up to work with different entity types...
  $entity_info = entity_get_info();
  $bundles_info = $entity_info['node']['bundles'];
  $label = $bundles_info[$bundleAdjusted]['label'];

  drupal_set_title($label);

  $formToReturn = array(
    '#bundle' => $bundleAdjusted,
  );

  $component = _form_logic_rule_table($bundleAdjusted, current_path());

  $formToReturn['rules_list'] = $component;

  $formToReturn['submit'] =
      array(
        '#type' => 'submit',
        '#value' => t('Save settings'),
  );


  $formToReturn['#submit'] =
      array(
        'form_logic_admin_form_submit');

  return $formToReturn;
}

/**
 * Form submission handler for form_logic_rule_list_form().
 * 
 * @package AdminInterface
 * 
 */
function form_logic_admin_form_submit($form, &$form_state) {
  
  // this needs to check for re-ordering
  // but (at present) everything else is already saved at this stage

  $bundle = $form['#bundle'];

  $rules_list = $form_state['values']['rules_list'];
  
  function _form_logic_compare_weight($a, $b) {
    return $a['weight'] > $b['weight'];
  }

  usort($rules_list, "_form_logic_compare_weight");

  $rule_list_to_save = array();
  $saved_rules = variable_get('form_logic_' . $bundle, array());

  foreach ($rules_list as $key => $value) {
    $rule_list_to_save[$key] = $saved_rules[$value['rid']];
//    $rule_list_to_save[$key]['more']['enabled'] = $value['enabled'];
  }

  if ($saved_rules == $rule_list_to_save) {
    drupal_set_message(t('Rules already saved'));
  }
  else {
    variable_set('form_logic_' . $bundle, $rule_list_to_save);
    drupal_set_message(t('Changes to rules saved'));
  }
}



/**
 * Returns HTML for the draggable list of behaviours
 *
 * @package RuleEditing
 * @subpackage TableSupport
 * 
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_form_logic_admin_form($variables) {
  $form = $variables['form'];
  $output = drupal_render($form['heading']);
  
  $rules_list = $form['rules_list'];
  $form_rows = element_children($rules_list);
  $rows = array();

  foreach ($form_rows as $id) {
    
    $form['rules_list'][$id]['weight']['#attributes']['class'] =
        array('rule-list-order-weight');

    $rows[] =
        array(
          'data' => array(
            drupal_render($form['rules_list'][$id]['summary']),
            //drupal_render($form['rules_list'][$id]['enabled']),//
            drupal_render($form['rules_list'][$id]['operations']),
            drupal_render($form['rules_list'][$id]['weight']),
          ),
          'class' => array(
            'draggable', 'form_logic_table_row'),
    );
  }

  $header = array(
    array('data' => t('Rule'), 'class' => 'form_logic_list_first_column'),
    //t('Enabled'),
    t('Operations'),
    t('Weight'),
  );

   $theme_parameter = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'rule-list-order'),
  );

  $output .= theme('table', $theme_parameter);

  drupal_add_tabledrag('rule-list-order', 'order', 'sibling', 'rule-list-order-weight');

  $output .= drupal_render_children($form);

  return $output;
}


/**
 * Rule table
 *
 * 
 *  
 * Return a from with a list of the rules associated with the bundle.
 * Elsewhere a theme function will turn the form into a draggable table.
 * Set up links with an return destination (optional)
 * 
 * @package RuleEditing
 * @subpackage TableSupport
 * 
 * @param type $bundle
 * @return boolean
 */
function _form_logic_rule_table($bundle) {
  $rules = variable_get('form_logic_' . $bundle, array());
 
  foreach ($rules as $key => $rule) {
    $form_component[$key] =
        _form_logic_rule_table_row($rule, $key, $bundle);
  }

  $form_component['#tree'] = TRUE;
  return $form_component;
}



/**
 * Return a specific table row
 * 
 * @package RuleEditing
 * @subpackage TableSupport
 * 
 * @return array specification of the row
 */

function _form_logic_rule_table_row($rule, $key, $bundle, 
    $destination = NULL) {
  
  module_load_include('inc', 'form_logic', 'form_logic.summary');
  module_load_include('inc', 'form_logic', 'form_logic.built_in_options');
  
  $summary_string =
      _form_logic_summary_for_rule($rule, $bundle);
  
 $link_base = base_path() . 'admin/structure/types/manage/'
     . $bundle . '/form_logic';
 
  $edit_link = $link_base . '/edit/' . $key;
  $delete_link = $link_base . '/delete/' . $key;
  $enable_link = $link_base . '/enable/' . $key;

  if ($destination) {
    $edit_link .= '?destination=' . $destination;
    $delete_link .= '?destination=' . $destination;
  }

  // quick CSS hack - would be better to put them in separate columns
  $edit_link = '<span style="margin-right: 5px;"><a href="' . $edit_link . '">edit</a></span>';
  $delete_link = '<span style="margin-left: 5px;"><a href="' . $delete_link . '">delete</a></span>';
  
  
  $enable_label = $rule['more']['enabled'] ? t('disable') : t('enable');
  
  $enable_link = '<span style="margin-left: 5px; margin-right: 5px;">' 
      . '<a href="'
      . $enable_link
      . '">'
      . $enable_label
      . '</a></span>';
  
  
  $row =
      array(
        // 'rule_key' => array('#markup' => $key),
        'rid' => array(
          '#type' => 'hidden',
          '#value' => $key,
        ),
//        'enabled' => array(
//          '#type' => 'checkbox',
//          '#value' => $enabled,
//        ),
       
        'summary' => array(
          '#markup' => $summary_string
        ),
        'weight' => array(
          '#type' => 'weight',
          '#default_value' => $key * 10,
          '#delta' => 50,
          '#title_display' => 'invisible',
        ),
        'operations' => array(
          array(
            '#markup' => $edit_link,
          ),
           array(
            '#markup' => $enable_link,
          ),
          array(
            '#markup' => $delete_link,
          ),
        ),
        '#attributes' => array('class' => array('form_logic_table_row')),
  );

  return $row;
}
