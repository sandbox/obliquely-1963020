<?php

/*
 * form_logic.configure
 *
 * Add / Edit / Delete forms for Form Logic Rules
 *
 *
 */

/**
 * Form: Add new rule to a given bundle
 *
 * @package RuleEditing
 * @subpackage Forms
 *
 * @param type $form
 * @param type $form_state
 * @param type $bundle
 * @return type
 */
function form_logic_add_rule_form($form, $form_state, $bundle) {

  $bundleAdjusted = str_replace('-', '_', $bundle);

  $bundle_rules = variable_get('form_logic_' . $bundleAdjusted, array());
  // generate a rule id that is highest current rid +1
  $rid = 0;
  if (count($bundle_rules) > 0) {
    $rid = max(array_keys($bundle_rules)) + 1;
  }

  return form_logic_edit_rule_form($form, $form_state, $bundle, $rid);
}

/**
 * Form: Edit an existing rule on a given bundle
 *
 * @package RuleEditing
 * @subpackage Forms
 *
 * @param type $form
 * @param type $form_state
 * @param type $bundle
 * @return type
 */
function form_logic_edit_rule_form($form, $form_state, $bundle, $rid) {

  $bundleAdjusted = str_replace('-', '_', $bundle);

  $formToReturn = array(
    '#bundle' => $bundleAdjusted,
    '#entity_type' => 'node',
    '#rid' => $rid,
    '#tree' => TRUE, // will break everything!
  );

  _form_logic_edit_form_main_body($formToReturn, $form_state, $rid);

  $formToReturn['submission']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save Rule'),
  );

  $formToReturn['#validate'] = array();
  $formToReturn['#submit'] = array('form_logic_edit_rule_form_submit');

  return $formToReturn;
}

/**
 * Form: Delete an existing rule on a given bundle
 *
 * @package RuleEditing
 * @subpackage Forms
 *
 * @param type $form
 * @param type $form_state
 * @param type $bundle
 * @return type
 */
function form_logic_delete_rule_form($form, &$form_state, $bundle, $rid) {

  module_load_include('inc', 'form_logic', 'form_logic.summary');

  $bundleAdjusted = str_replace('-', '_', $bundle);

  $bundle_rules = variable_get('form_logic_' . $bundleAdjusted, array());

  $formToReturn = array(
    '#bundle' => $bundle,
    '#entity_type' => 'node',
    '#rid' => $rid,
  );


  $rule_exists = isset($bundle_rules[$rid]);

  $markup = t('The selected rule does not exist.') . " ($rid)";

  if ($rule_exists) {
    $rule = $bundle_rules[$rid];
    $markup = _form_logic_summary_for_rule($rule, $bundle);
  }

  $formToReturn['delete_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => 'Rule to delete',
    'rule' => array(
      '#markup' => $markup,
    ),
  );


  if ($rule_exists) {
    $formToReturn['actions']['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Delete'),
          '#name' => 'delete',
    );
  }

  $formToReturn['actions']['cancel'] = array(
        '#type' => 'button',
        '#value' => t('Cancel'),
        '#executes_submit_callback' => TRUE,
        '#name' => 'cancel',
  );

  $formToReturn['#submit'] = array(
        'form_logic_delete_rule_form_submit');

  return $formToReturn;
}

/**
 * Submit edit rule form
 *
 * @package RuleEditing
 * @subpackage Forms
 *
 * @param type $form
 * @param type $form_state
 * @param type $bundle
 * @return type
 */
function form_logic_edit_rule_form_submit(&$form, &$form_state) {
  $rid = $form['#rid'];
  $current = $form_state['values'];

  // clean up
  unset($current['conditions']['add']);
  unset($current['actions']['add']);

  $bundle_rules = variable_get('form_logic_' . $form['#bundle'], array());
  $bundle_rules[$rid] = $current;
  variable_set('form_logic_' . $form['#bundle'], $bundle_rules);

  $form_state['redirect'] = 'admin/structure/types/manage/' .
      $form['#bundle'] . '/form_logic';
}

/**
 * Submit delete rule form
 *
 * @package RuleEditing
 * @subpackage Forms
 *
 * @param type $form
 * @param type $form_state
 * @param type $bundle
 * @return type
 */
function form_logic_delete_rule_form_submit($form, &$form_state) {

  $bundle = $form['#bundle'];
  $form_state['redirect'] = 'admin/structure/types/manage/' .
      $bundle . '/form_logic';

// $entity_type = $form['#entity_type'];
  $rid = $form['#rid'];

  if ($form_state['clicked_button']['#name'] == 'cancel') {
    return;
  }

  $bundle_rules = variable_get('form_logic_' . $bundle, array());
  unset($bundle_rules[$rid]);
  variable_set('form_logic_' . $form['#bundle'], $bundle_rules);
}

/**
 *
 * Create the body of the edit form
 *
 * @package RuleEditing
 * @package SupportingFunctions
 *
 * @param type $form
 * @param type $form_state
 * @param type $rid
 * @return type
 */
function _form_logic_edit_form_main_body(&$form, $form_state, $rid) {

  $bundle = $form['#bundle'];
  $node_info = entity_get_info('node');
  $bundles = $node_info['bundles'];
  $label = $bundles[$bundle]['label'];

  drupal_set_title($label . ' (Form Logic)');

  $bundle_rules = variable_get('form_logic_' . $form['#bundle'], array());

  $rule = array();

  if ($form_state['rebuild'] == TRUE) {
    $rule = $form_state['values'];
    
    // check for additions or removals
    $clicked = $form_state['clicked_button']['#name'];

    if ($clicked == 'remove_action') {
      $id = $form_state['clicked_button']['#id'];
      unset($rule['actions'][$id]);
    }
    elseif ($clicked == 'add_action') {
      $rule['actions'][] = array(
        'action' => 'no_op',
      );
    }
    elseif ($clicked == 'remove_condition') {
      $id = $form_state['clicked_button']['#id'];
      unset($rule['conditions'][$id]);
    }
    elseif ($clicked == 'add_condition') {
      $rule['conditions'][] = array(
        'condition' => 'always',
      );
    }
  }
  elseif (isset($bundle_rules[$rid])) {
    // grab the current rule
    $rule = $bundle_rules[$rid];
  }
  else {
    // default rule
    $conditions[0] = array('condition' => 'always');
    $actions[0] = array('action' => 'no_op');
    $extras = array(
      'guard_clause' => FALSE,
      'name' => '',
      'show_name_only' => FALSE,
    );
    $rule = array(
      'conditions' => $conditions,
      'actions' => $actions,
      'more' => $extras,
    );
  }

  $form['conditions'] = _form_logic_conditions_form_component($rule, $form,
      $form_state);

  $form['actions'] = _form_logic_actions_form_component($rule, $form,
      $form_state);

  $form['more'] = _form_logic_more_component($rule);

  return $form;
}

/**
 *
 * Create the More section of the edit form
 *
 * @package RuleEditing
 * @package SupportingFunctions
 *
 
 * @param type $rule
 * @return type
 */
function _form_logic_more_component($rule) {

  $default_guard = $rule['more']['guard_clause'];
  $default_name = $rule['more']['name'];
  $default_show_name_only = $rule['more']['show_name_only'];

  $default_enabled = TRUE;
  if (isset($rule['more']['enabled'])) {
    $default_enabled = $rule['more']['enabled'];
  }

  return array(
    '#type' => 'fieldset',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#title' => 'More',
    'enabled' =>
    array(
      '#type' => 'checkbox',
      '#default_value' => $default_enabled,
      '#title' => t('Enable rule'),
    ),
    'guard_clause' => array(
      '#type' => 'checkbox',
      '#default_value' => $default_guard,
      '#title' => t('Bail if this rule succeeds, '
          . 'i.e. do not check any subsequent rules if the condition is'
          . 'met and the action executes.'),
    ),
    'name' => array(
      '#type' => 'textfield',
      '#default_value' => $default_name,
      '#title' => t('Name for this rule'),
    ),
    'show_name_only' => array(
      '#type' => 'checkbox',
      '#default_value' => $default_show_name_only,
      '#title' => t('Only show this name in listings.'),
    ),
  );
}

/**
 * Create the Conditions section of the edit form
 * 
 * @package RuleEditing
 * @package SupportingFunctions
 *
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @return type
 */

function _form_logic_conditions_form_component($rule, $form, $form_state) {

  module_load_include('inc', 'form_logic', 'form_logic.built_in_options');

  $fieldset_title = t('Conditions');

  $all_conditions = _form_logic_available_conditions_info();
  $condition_options = array();

  foreach ($all_conditions as $key => $value) {
    $condition_options[$key] = $value['title'];
  }

  $condition_sub_rules = $rule['conditions'];
  unset($condition_sub_rules['add']);


  if (count($condition_sub_rules) == 0) {
    $condition_sub_rules['0']['0'] = array('condition' => 'always');
  }

  $conditions = array();
  foreach ($condition_sub_rules as $cid => $sub_rule) {

    $default = $sub_rule['condition'];

    if (!isset($sub_rule['condition'])) {
      $default = 'always';
    }

    $conditions[$cid]['condition'] = array(
      '#type' => 'select',
      '#title' => t('Apply when'),
      '#options' => $condition_options,
      '#default_value' => $default,
      '#required' => TRUE,
      '#ajax' => _form_logic_ajax_for_type('condition'),
      '#attributes' => array('class' => array('form_logic_select_minimmum_width')),
    );


    // get specification array for the current condition
    $conditions_info = _form_logic_available_conditions_info();

    $condition_info = $conditions_info[$sub_rule['condition']];

    // display additional parameter fields if required
    if (isset($condition_info['parameters'])) {

      foreach ($condition_info['parameters'] as $parameter) {

        $callback = '_form_logic_parameter_' . $parameter;

        if (_form_logic_function_check($callback)) {
          $conditions[$cid][$parameter] = $callback($sub_rule, $form,
              $form_state, 'condition');
        }
      }
    }
  }

  // outer wrapper for all the conditions

  $conditions_component = array(
    '#type' => 'fieldset',
    '#collapsed' => FALSE, // TRUE,
    '#collapsible' => TRUE, // TRUE,
    '#title' => $fieldset_title,
    '#prefix' => '<div id=form_logic_rule_conditions>',
    '#suffix' => '</div>',
  );

  $count = 0;
  foreach ($conditions as $key => $condition) {
    $display_key = $key + 1;
    $condition['#type'] = 'fieldset';
    $condition['#title'] = 'Condition ' . $display_key;
    $condition['#collapsible'] = TRUE;
    $condition['#collapsed'] = FALSE;
    $condition['#prefix'] = '<div class=form_logic_condition>';
    $condition['#suffix'] = '</div>';

    if ($count != 0) {

      $condition['remove'] = array(
        '#type' => 'button',
        '#value' => t('Remove condition'),
        '#name' => 'remove_condition',
        '#id' => $key,
        '#ajax' => array(
          '#submit' => array(
            '_form_logic_edit_rule_rebuild_conditions_submit'),
          'callback' => '_form_logic_edit_rule_rebuild_conditions_js',
          'wrapper' => 'form_logic_rule_conditions',
        ),
        '#attributes' => array('class' => array('form_logic_smaller_button')),
        '#prefix' => '<div class=form_logic_remove_condition>',
        '#suffix' => '</div>'
      );
    }
    $conditions_component[] = $condition;
    $count++;
  }

  $conditions_component['add'] = array(
    '#type' => 'button',
    '#value' => t('Add condition'),
    '#name' => 'add_condition',
    '#ajax' => array(
      '#submit' => array(
        '_form_logic_edit_rule_rebuild_conditions_submit'),
      'callback' => '_form_logic_edit_rule_rebuild_conditions_js',
      'wrapper' => 'form_logic_rule_conditions',
    ),
    '#attributes' => array('class' => array('form_logic_smaller_button')),
  );

  return $conditions_component;
}


/**
 *
 * Create the Action section of the edit form
 *
 * @package RuleEditing
 * @package SupportingFunctions
 *
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @return type
 */

function _form_logic_actions_form_component($rule, &$form, $form_state) {

  $fieldset_title = t('Actions');

  $all_actions = _form_logic_available_actions_info();

  $action_options = array();

  foreach ($all_actions as $key => $value) {
    $action_options[$key] = $value['title'];
  }

  $actions = array();

  $action_sub_rules = $rule['actions'];
  unset($action_sub_rules['add']);

  foreach ($action_sub_rules as $cid => $sub_rule) {

    $default = $sub_rule['action'];

    if (!isset($sub_rule['action'])) {
      dpm('action ' . $cid . ' form rule is missing. Setting default to no op.');
      $default = 'no_op';
    }

    $actions[$cid]['action'] = array(
      '#type' => 'select',
      '#title' => t('Action'),
      '#options' => $action_options,
      '#default_value' => $default,
      '#required' => TRUE,
      '#ajax' => _form_logic_ajax_for_type('action'),
      '#attributes' => array('class' => array('form_logic_select_minimmum_width')),
    );

    $actions_info = _form_logic_available_actions_info();
    $action_info = $actions_info[$sub_rule['action']];

    // display additional parameter fields if required
    if (isset($action_info['parameters'])) {

      foreach ($action_info['parameters'] as $parameter) {

        $callback = '_form_logic_parameter_' . $parameter;

        if (_form_logic_function_check($callback)) {
          $actions[$cid][$parameter] = $callback($sub_rule, $form, $form_state,
              'action');
        }
      }
    }
  }

  // wrapper for actions
  $actions_component = array(
    '#type' => 'fieldset',
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
    '#title' => $fieldset_title,
    '#prefix' => '<div id=form_logic_rule_actions>',
    '#suffix' => '</div>',
  );

  $count = 0;
  foreach ($actions as $key => $action) {
    $display_key = $key + 1;
    $action['#type'] = 'fieldset';
    $action['#title'] = 'Action ' . $display_key;
    $action['#collapsible'] = TRUE;
    $action['#collapsed'] = FALSE;

    if ($count != 0) {

      $action['remove'] = array(
        '#type' => 'button',
        '#value' => t('Remove action'),
        '#name' => 'remove_action',
        '#id' => $key,
        '#ajax' => _form_logic_ajax_for_type('action'),
        '#attributes' => array('class' => array('form_logic_smaller_button')),
      );
    }

    $actions_component[] = $action;
    $count++;
  }

  $actions_component['add'] = array(
    '#type' => 'button',
    '#value' => t('Add action'),
    '#name' => 'add_action',
    '#ajax' => array(
      '#submit' => array(
        '_form_logic_edit_rule_rebuild_actions_submit'),
      'callback' => '_form_logic_edit_rule_rebuild_actions_js',
      'wrapper' => 'form_logic_rule_actions',
    ),
    '#attributes' => array('class' => array('form_logic_smaller_button')),
  );

  return $actions_component;
}


/**
 * Create the matching options drop down list
 * 
 * This is for the standard case (not the term reference case)
 *
 * @package RuleEditing
 * @package SupportingFunctions
 *
 * 
 * @param type $rule
 * @param type $type
 * @return type
 */
function _form_logic_match_option_form_component($rule, $type) {
 
  $match_options = _form_logic_match_options();
  $options = array();

  foreach ($match_options as $key => $value) {
    $options[$key] = $value['name'];
  }

  $default_value = isset($rule['match_option']) ? $rule['match_option'] : 0;

  return array(
    '#type' => 'select',
    '#title' => t('Match option'),
    '#multiple' => FALSE,
    '#options' => $options,
    '#default_value' => $default_value,
    '#ajax' => _form_logic_ajax_for_type($type),
  );
}


/**
 * Submit function for Condition section (required by AJAX?)
 * 
 * @package RuleEditing
 * @package SupportingFunctions
 * 
 * @param type $form
 * @param array $form_state
 */
function _form_logic_edit_rule_rebuild_conditions_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Rebuild function for Condition section (required by AJAX?)
 * 
 * @package RuleEditing
 * @package SupportingFunctions
 * 
 * @param type $form
 * @param array $form_state
 */
function _form_logic_edit_rule_rebuild_conditions_js($form, &$form_state) {
  return $form['conditions'];
}

/**
 * Submit function for Action section (required by AJAX?)
 * 
 * @package RuleEditing
 * @package SupportingFunctions
 * 
 * @param type $form
 * @param array $form_state
 */
function _form_logic_edit_rule_rebuild_actions_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Rebuild function for Action section (required by AJAX?)
 * 
 * @package RuleEditing
 * @package SupportingFunctions
 * 
 * @param type $form
 * @param array $form_state
 */
function _form_logic_edit_rule_rebuild_actions_js($form, &$form_state) {
  return $form['actions'];
}

/**
 * Provide match options list, text box, and case sensitivity check box
 * for the match_content parameter.
 *
 * Implements _form_logic_parameter_PARAMETER_TYPE for 'match_content'
 * 
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @param type $type
 * @return type
 */
function _form_logic_parameter_match_content($rule, $form, $form_state, $type) {

  $match_types = _form_logic_match_options();
  $options = array();

  foreach ($match_types as $key => $value) {
    $options[$key] = $value['name'];
  }

  $default = 'empty';
  if (isset($rule['match_content']['match_type'])) {
    $default = $rule['match_content']['match_type'];
  }

  $form_component['match_type'] = array(
    '#type' => 'select',
    '#title' => 'Match type',
    '#default_value' => $default,
    '#options' => $options,
    '#ajax' => _form_logic_ajax_for_type($type),
  );

  // now check if we need to add a parameter element to the form
  $match_type = $match_types[$default];

  if (!isset($match_type['parameters'])) {
    return $form_component;
  }

  foreach ($match_type['parameters'] as $parameter) {
    $callback = '_form_logic_match_parameter_' . $parameter;

    if (_form_logic_function_check($callback)) {
      $form_component['match_parameter_' . $parameter] = $callback($rule, $form,
          $form_state);
    }
  }

  return $form_component;
}


/**
 * Provide a select control for the match_content parameter.
 *
 * Implements _form_logic_parameter_PARAMETER_TYPE for 'match_content'
 * 
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @param type $type
 * @return type
 */
function _form_logic_parameter_on_off($rule, $form, $form_state, $type) {

  $options = array();
  $options[0] = 'Off / False / Un-ticked';
  $options[1] = 'On / True / Ticked';
  
  
  $default = 0;
  if (isset($rule['on_off'])) {
    $default = $rule['on_off'];
  }

  $form_component['on_off_state'] = array(
    '#type' => 'select',
    '#title' => 'State',
    '#default_value' => $default,
    '#options' => $options,
    '#ajax' => _form_logic_ajax_for_type($type),
  );


  return $form_component;
}

/**
 * Provides a text field for match_parameter_text
 * 
 *
 * Implements _form_logic_match_parameter_SUBTYPE
 * 
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @return type
 */

function _form_logic_match_parameter_text($rule, $form, $form_state) {
  $default = '';

  if (isset($rule['match_content']['match_parameter_text'])) {
    $default = $rule['match_content']['match_parameter_text'];
  }

  $parameter_component = array(
    '#type' => 'textfield',
    '#description' => 'Match string',
    '#default_value' => $default,
  );

  return $parameter_component;
}

/**
 * Return a textarea for the message to display
 * 
 * Implements form_logic_parameter_PARAMETER_NAME
 * 
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @todo add token support
 * @todo add HTML support
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @return type
 */
function _form_logic_parameter_message_text($rule, $form, $form_state) {

  return array(
    '#type' => 'textarea',
    '#title' => 'Message',
    '#required' => TRUE,
    '#default_value' => $rule['message_text'],
    '#description' => t('No tokens just now. HTML is OK. There isn\'t filtering just yet, but there needs to be.'),
  );
}

/**
  * Return a check box for the case sensitivity of text comparison
 * 
 * Implements form_logic_match+parameter_SUBTYPE
 * 
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @todo add token support
 * @todo add HTML support
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @return type
 */
function _form_logic_match_parameter_case($rule, $form, $form_state) {

  $default = FALSE;

  if (isset($rule['match_content']['match_parameter_case'])) {
    $default = $rule['match_content']['match_parameter_case'];
  }

  $parameter_component = array(
    '#type' => 'checkbox',
    '#title' => 'Case sensitive',
    '#default_value' => $default,
  );

  return $parameter_component;
}


/**
 * Return a selection list for term reference matching options
 * 
 * Implements form_logic_parameter_PARAMETER_NAME
 * 
 * 
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @param type $type
 * @return type
 */
function _form_logic_parameter_terms_options($rule, $form, $form_state, $type) {
  $match_types = _form_logic_taxonomy_match_options();

  $options = array();

  foreach ($match_types as $key => $value) {
    $options[$key] = $value['name'];
  }

  $default = isset($rule['terms_options']) ? $rule['terms_options'] : array();

  return array(
    '#type' => 'select',
    '#title' => t('Options'),
    '#options' => $options,
    '#default_value' => $default,
    '#attributes' => array('class' => array('form_logic_select_minimmum_width')),
  );
}

/**
 * Returns a selection list of fields that can be target of a term reference
 * condtion
 * 
 * Implements form_logic_parameter_PARAMETER_NAME
 * 
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @param type $type
 * @return type
 */
function _form_logic_parameter_terms($rule, $form, $form_state, $type) {

  $field = field_info_field($rule['target']);

  $default = NULL;
  if (isset($rule['terms'])) {
    $default = $rule['terms'];
  }

  return _form_logic_select_terms_for_field($field, $default);
}

/**
 * A selector for terms associated with a field.
 * 
 * 
 * Used by:
 * _form_logic_parameter_check_terms
 * _form_logic_parameter_limit_terms
 *
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 */

function _form_logic_select_terms_for_field($field, $default) {

  $vocab_machine_name = $field['settings']['allowed_values'][0]['vocabulary'];

  $vob = taxonomy_vocabulary_machine_name_load($vocab_machine_name);

  $vocab_objects = taxonomy_get_tree($vob->vid);

  $terms = array();

  foreach ($vocab_objects as $vocab_object) {
    $terms[$vocab_object->tid] = $vocab_object->name;
  }

  $size = count($terms);

  if ($size > 10) {
    $size = 10;
  }

  return array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Terms'),
    '#options' => $terms,
    '#default_value' => $default,
    '#size' => $size,
    '#attributes' => array('class' => array('form_logic_select_minimmum_width')),
  );
}

/**
 * Returns a date popup for setting a date range
 * 
 * Implements form_logic_parameter_PARAMETER_NAME
 * 
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @param type $type
 * @return type
 */

function _form_logic_parameter_date_range($rule, $form, $form_state) {

  $popup_extra = array();

  if (function_exists('date_popup_date_format')) {

    $popup_extra = array(
      '#type' => 'date_popup',
      '#date_format' => variable_get('date_format_medium', 'd M Y - H:i'),
      '#date_year_range' => '-100:+50',
    );
  }

  $default_earliest = isset($rule['earliest_date']) ? $rule['earliest_date'] : '';
  $default_latest = isset($rule['latest_date']) ? $rule['latest_date'] : '';

  $earliest_date = $popup_extra + array(
    '#type' => 'date',
    '#title' => 'Earliest date',
    '#default_value' => $default_earliest,
    '#description' => t('Leave blank for no earliest date'),
  );

  $latest_date = $popup_extra + array(
    '#type' => 'date',
    '#title' => 'Latest date',
    '#default_value' => $default_latest,
    '#description' => t('Leave blank for no latest date'),
  );

  return array(
    'earliest_date' => $earliest_date,
    'latest_date' => $latest_date,
  );
}


/**
 * Returns a pair of text fields for setting a numerical range
 * condtion
 * 
 * Implements form_logic_parameter_PARAMETER_NAME
 * 
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @param type $type
 * @return type
 */
function _form_logic_parameter_numerical_range($rule, $form, $form_state, $type) {

  $lowest_value = array(
    '#type' => 'textfield',
    '#title' => 'Lowest value',
    '#default_value' => isset($rule['lower']) ? $rule['lowest_value'] : '',
    '#description' => t('Leave blank for no lower limit'),
  );

  $highest_value = array(
    '#type' => 'textfield',
    '#title' => 'Highest value',
    '#default_value' => isset($rule['upper']) ? $rule['highest_value'] : '',
    '#description' => t('Leave blank for no upper limit'),
  );

  return array(
    'lower' => $lowest_value,
    'upper' => $highest_value,
  );
}


/**
 * Returns a selection list of roles 
 * 
 * Implements form_logic_parameter_PARAMETER_NAME
 * 
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @param type $type
 * @return type
 */
function _form_logic_parameter_roles($rule, $form, $form_state, $type) {
  $default = isset($rule['roles']) ? $rule['roles'] : array();

  return array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Select roles'),
    '#options' => user_roles(),
    '#default_value' => $default,
    '#attributes' => array('class' => array('form_logic_select_minimmum_width')),
  );
}

/**
 * Return a form component for selecting target for the action
 *
 * Implements form_logic_parameter_PARAMETER_NAME
 * 
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @params
 * - rule
 * - form
 * - form_state
 * - type (string) 'action' or 'condition'
 *
 */

function _form_logic_parameter_target(&$rule, $form, $form_state, $type) {

  // generate list of suitable targets
  $instances = field_info_instances('node', $form['#bundle']);

  $current_action = _form_logic_element_info($rule);

  $valid_target_types = NULL;

  if (isset($current_action['valid_target_types'])) {
    $valid_target_types = $current_action['valid_target_types'];
  }

  $targets = array();

  foreach ($instances as $field_name => $instance) {

    $field = field_info_field($field_name);
    // include ONLY IF a member of the valid_target_types array
    if (isset($valid_target_types)) {

      if (!_form_logic_target_valid($field['type'], $valid_target_types)) {
        continue;
      }
    }
    $targets[$field_name] = $instance['label'];
  }

  if (count($targets) == 0) {
    return array(
      '#markup' => '<p><em>' . t('No target field available') . '</em></p>',
    );
  }

  // default target
  $target_keys = array_keys($targets);
  $default_target = reset($target_keys); // default to first in list
  // override default if we already have a value AND it's a legal one
  if (isset($rule['target']) && array_key_exists($rule['target'], $targets)) {
    $default_target = $rule['target'];
  }
  // otherwise, set the default in the rule
  else {
    $rule['target'] = $default_target;
  }

  return array(
    '#type' => 'select',
    '#multiple' => FALSE,
    '#title' => t('Target field'),
    '#options' => $targets,
    '#default_value' => $default_target,
    '#ajax' => _form_logic_ajax_for_type($type),
    '#attributes' => array('class' => array('form_logic_select_minimmum_width')),
  );
}

/**
 * Fetch ajax part of form for action or condition
 *
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @param (string) type - either 'action' or 'condition'
 *
 */

function _form_logic_ajax_for_type($type) {
  if ($type != 'condition' && $type != 'action') {
    dpm('_form_logic_ajax_for_type has invalid parameter \'' . $type . '\'');
    return;
  }

  if ($type == 'action') {
    return array(
      '#submit' => array('_form_logic_edit_rule_rebuild_actions_submit'),
      'callback' => '_form_logic_edit_rule_rebuild_actions_js',
      'wrapper' => 'form_logic_rule_actions',
    );
  }

  return array(
    '#submit' => array('_form_logic_edit_rule_rebuild_conditions_submit'),
    'callback' => '_form_logic_edit_rule_rebuild_conditions_js',
    'wrapper' => 'form_logic_rule_conditions',
  );
}

/**
 * Show select list for the selected target and provide term options.
 *
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @return type
 * 
 *
 */

function _form_logic_parameter_limit_terms($rule, $form, $form_state) {

  $default = isset($rule['limit_terms']) ? $rule['limit_terms'] : array();

  $field = field_info_field($rule['target']);
  return _form_logic_select_terms_for_field($field, $default);
}

/**
 *  Check if a field is a valid target
 *
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @param string $field_type
 *    the field type
 * @param array $valid_types
 *    an array of valid types - allows wildcards of the form number*
 */

function _form_logic_target_valid($field_type, $valid_types) {
  // have to iterate manually in order to cope with wildcard case
  // otherwise could just use in_array()

  foreach ($valid_types as $valid_type) {

    if (substr($valid_type, -1) === '*') {
      $valid_type = trim($valid_type, '*');

      if ($valid_type === substr($field_type, 0, strlen($valid_type))) {
        return TRUE;
      }
      continue;
    }

    if ($valid_type === $field_type) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Show select list for formats
 * 
 * **Not currently used**
 * 
 * @package RuleEditing
 * @subpackage SupportingFunctions
 * 
 * @param type $rule
 * @param type $form
 * @param type $form_state
 * @param type $type
 * @return type
 */
function _form_logic_parameter_formats(&$rule, $form, $form_state, $type) {

  // generate list of possible formats
  $formats = filter_formats();
  dpm($formats, 'formats');

  $format_options = array();

  foreach ($formats as $key => $format) {
    $format_options[$key] = $format->name;
  }

  $size = count($format_options);

  if ($size > 10) {
    $size = 10;
  }

  $default = array();
  if (isset($rule['formats'])) {
    $default = $rule['formats'];
  }

  return array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Formats'),
    '#options' => $format_options,
    '#default_value' => $default,
    '#size' => $size,
    '#attributes' => array('class' => array('form_logic_select_minimmum_width')),
  );
}
