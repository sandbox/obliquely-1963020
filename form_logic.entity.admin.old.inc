<?php

/**
 * @file
 * Implements a form for the Form Logic settings for an entity
 * Provides overview of settings for all fields on the entity
 */

/**
 *
 * Path: BUNDLE_ADMIN_PATH/form_logic, where BUNDLE_ADMIN_PATH is
 * the path stored in the ['admin']['info'] property in the return value of
 * hook_entity_info().
 *
 * @ingroup forms
 */

function form_logic_entity_form($form, &$form_state, $object) {

  // include for the _form_logic_row_rule function
  module_load_include('inc', 'form_logic', 'form_logic.admin');

  // what kind of object is $object?
  
  drupal_set_title($object->name);

  $bundle = $object->type;  // OK for nodes - not OK for other entities

  $form = array(
        '#bundle' => $bundle,
  );

  
  $bundle_rules = variable_get('form_logic_' . $bundle, array());
  
  $rule_count = count($bundle_rules);
  
  $form['testing'] = array(
    '#markup' => '<h2>Testing variable_set() / variable_get() approach</h2>'
    . '<p>We found: ' . $rule_count . ' bundle rules',
  );
  
  $form['development_comment'] = array(
    '#markup' => '<p><em>Work in progress. No dragging or saving on this ' 
    . 'form just yet.</em></p>',
    '#weight' => -5,
  );

  // figure out the fields associated with this entity
  // works for taxonomies and nodes
  // but does not work for other entity types
  // clearly, I'm doing it wrong
  // fix for taxonomies - their object structure is different
//  if ($entity_type == 'taxonomy') {
//    $entity_type = 'taxonomy_term'; // fix for taxonomy 
//    $bundle = $entity->machine_name;
//  }

  $fields = field_info_instances('node', $bundle);
  
  foreach ($fields as $field_key => $field_value) {
    $form['fields'][$field_key]['heading'] = array(
      '#markup' => '<a name="' . urlencode($field_value['label']) . '"></a><h3>' . $field_value['label'] . '</h3>',
    );

    $instance = field_info_instance('node', $field_key, $bundle);

    $rules = $instance['form_logic_settings'];

    // return path with anchor (for cases where we have many fields)
    // the anchor is passed OK, but doesn't seem to be effective when
    // the form returns - not critical - look at another time
    $return_path =  current_path() . '#' . urlencode($field_value['label']);
    
    $component = _form_logic_rules_list_form_component($rules, $bundle,
        $field_key, $return_path);

    $form['fields'][$field_key]['rules_list'] = $component;
  }

//  $form['submit'] =
//      array(
//        '#type' => 'submit',
//        '#value' => t('Save settings'),
//  );
//
//
//  $form['#submit'] =
//      array(
//        'form_logic_entity_form_submit');

  return $form;
}

/**
 * Returns HTML for the draggable list of behaviours
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_form_logic_entity_form($variables) {
  $form = $variables['form'];
  $output = '';
 
  $fields = element_children($form['fields']);

  foreach ($fields as $field_name) {

    $rules_list = $form['fields'][$field_name]['rules_list'];

    $form_rows = element_children($rules_list);

    $output .= render($form['fields'][$field_name]['heading']);

    $rows = array();
    
    foreach ($form_rows as $id) {

      if ($id === 'add_rules') {
        continue;
      }

      $form['fields'][$field_name]['rules_list'][$id]['weight']['#attributes']['class'] =
          array(
            'rule-list-order-weight');

      $rows[] =
          array(
            'data' => array(
              drupal_render($form['fields'][$field_name]['rules_list'][$id]['summary']),
              drupal_render($form['fields'][$field_name]['rules_list'][$id]['if_successful']),
              drupal_render($form['fields'][$field_name]['rules_list'][$id]['rule_key']),
              drupal_render($form['fields'][$field_name]['rules_list'][$id]['operations']),
              drupal_render($form['fields'][$field_name]['rules_list'][$id]['weight']),
            ),
            'class' => array(
              'draggable'),
      );
    }


    // now add one extra row to allow adding of additional rules
    $rows[] = array(
      'data' => array(
        array(
          'data' => drupal_render($form['fields'][$field_name]['rules_list']['add_rules']),
          'colspan' => 5,
          'class' => 'form_logic_rule_element_add',
        ),
      ),
    );

    $header = array(
      array('data' => t('Rule'), 'class' => 'form_logic_list_first_column'),
      t('If Rule Succeeds'),
      t('Rule Key'),
      t('Operations'),
      t('Weight'),
    );


    $output .=
        theme('table',
        array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(
        'id' => 'rule_list_' . $field_name)));

//      drupal_add_tabledrag('rule_list_' . $key, 'order', 'sibling',
//      'rule-list-order-weight');
  }

  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Form submission handler for form_logic_rule_list_form().
 */
function form_logic_entity_form_submit($form, &$form_state) {

  $bundle = $form['#bundle'];
  $entity_type = $form['#entity_type'];
  $field_name = $form['#field_name'];

  $instance =
      field_read_instance($entity_type, $field_name, $bundle);

  $rule_list = $form_state['values']['behaviour_list'];

  function _form_logic_compare_weight($a, $b) {
    return $a['weight'] >
        $b['weight'];
  }

  usort($rule_list, "_form_logic_compare_weight");

  $rule_list_to_save = array();

  foreach ($rule_list as $key => $value) {
    $rule_list_to_save[$key] = $instance['form_logic_settings'][$value['rid']];
  }

  $instance['form_logic_settings'] = $rule_list_to_save;

  try {
    field_update_instance($instance);
    drupal_set_message(t('Changed the FORM LOGIC settings for field %label.',
            array(
      '%label' => $instance['label'])));
  } catch (Exception $e) {
    drupal_set_message(t('There was a problem changing the FORM LOGIC settings for field %label.',
            array(
      '%label' => $instance['label'])), 'error');
  }


  // on submission redirect back to the list of form logic rules

  $bundle_for_path = $bundle = str_replace('_', '-', $bundle);

  $redirect = 'admin/structure/types/manage/' . $bundle_for_path . '/fields/' . $field_name . '/form_logic';

  $form_state['redirect'] = $redirect;
}

