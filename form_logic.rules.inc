<?php

/**
 * @file
 * Rules integration for Form Logic
 * NOT WRITTEN YET - this is field formatter conditions code untouched
 * use this code as a starting point - but the use case is far from identical
 */

/**
 * Implements hook_rules_data_info().
 * 
 * Work in Progress. Unlikely to do anything sane.
 * 
 * @package RulesModuleSupport
 * 
 */
function form_logic_rules_data_info() {
  return array(
    'form_logic_element' => array(
      'label' => t('form_logic element'),
      'wrap' => TRUE,
    ),
    'form_logic_field' => array(
      'label' => t('form_logic field'),
    ),
    'form_logic_empty' => array(
      'label' => t('form_logic empty parameters'),
    ),
  );
}

/**
 * Implements hook_rules_event_info().
 * 
 * Work in Progress. Unlikely to do anything sane.
 * 
 * @package RulesModuleSupport
 * 
 */
function form_logic_rules_event_info() {

  $base = array('variables' => array());
  $entity_info = entity_get_info();
  foreach ($entity_info as $entity_type => $info) {
    $base['variables'][$entity_type] = array('type' => $entity_type, 'label' => $info['label']);
  }

  return array(
    'field_is_rendered' => array(
      'label' => t('A field is rendered'),
      'group' => t('Field formatter conditions'),
    ) + $base,
  );
}

/**
 * Implements hook_rules_action_info().
 * 
 * Work in Progress. Unlikely to do anything sane.
 * 
 * @package RulesModuleSupport
 */
function form_logic_rules_action_info() {

  $base_parameters = array(
    'form_logic_element' => array(
      'type' => 'element',
      'label' => t('Element'),
      'optional' => TRUE,
    ),
    'form_logic_field' => array(
      'type' => 'element',
      'label' => t('Field'),
      'optional' => TRUE,
    ),
  );

  return array(
    'form_logic_rules_hide_fields' => array(
      'label' => t('Hide a field'),
      'group' => t('Field formatter conditions'),
      'parameter' => array() + $base_parameters,
    ),
    'form_logic_rules_change_image_style' => array(
      'label' => t('Change image style'),
      'group' => t('Field formatter conditions'),
      'parameter' => $base_parameters + array(
        'form_logic_empty' => array(
          'allow null' => TRUE,
          'optional' => TRUE,
          'type' => 'form_logic_empty',
          'label' => t('Image style'),
          'ui class' => 'RulesDataUIText',
          'options list' => 'form_logic_rules_change_image_style_options',
        ),
      ),
    ),
  );
}

/**
 * Rules options list for image styles.
 * 
 * Work in Progress. Unlikely to do anything sane.
 * 
 * @package RulesModuleSupport
 * 
 */
function form_logic_rules_change_image_style_options() {
  $options = array();
  $image_styles = image_styles();
  foreach (array_keys($image_styles) as $image_style) {
    $options[$image_style] = $image_style;
  }
  return $options;
}

/**
 * Rules callback: hide fields.
 * 
 * Work in Progress. Unlikely to do anything sane.
 * 
 * @package RulesModuleSupport
 * 
 */
function form_logic_rules_hide_fields($form_logic_element, $form_logic_field) {
  $form_logic_element->fired = TRUE;
  $form_logic_element->element[$form_logic_field]['#access'] = FALSE;
}

/**
 * Rules callback: change image style.
 * 
 * Work in Progress. Unlikely to do anything sane.
 * 
 * @package RulesModuleSupport
 * 
 */
function form_logic_rules_change_image_style($form_logic_element, $form_logic_field, $empty, $configuration) {
  $form_logic_element->fired = TRUE;
  foreach (element_children($form_logic_element->element[$form_logic_field]) as $key) {
    $form_logic_element->element[$form_logic_field][$key]['#image_style'] = $configuration['form_logic_empty'];
  }
}

/**
 * Helper function to hide our optional element and field data selectors.
 * 
 * Work in Progress. Unlikely to do anything sane.
 * 
 * @package RulesModuleSupport
 * 
 */
function _form_logic_form_rules_ui_element_alter(&$form) {
  if (isset($form['parameter']) && isset($form['parameter']['form_logic_element'])) {
    $form['parameter']['form_logic_element']['#access'] = FALSE;
    $form['parameter']['form_logic_field']['#access'] = FALSE;
  }
}
